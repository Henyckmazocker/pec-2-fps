PAC1 FPS David Carvajal Abellán

En esta PAC he desarrollado un juego shooter en primera persona.

Los controles son:
w,a,s,d: Controles direccionales.
Barra espaciadora: Salto.
Shift: Esprint.
Click izquierdo: Disparo.
Rueda del ratón: Cambio de arma.

En la pantalla de Fin de juego se deberá apretar Enter para reiniciar el juego.

Tareas realizadas:

La parte inicial del escenario deberá estar ubicada en un terreno montañoso, pudiendo también hacer partes urbanas, y luego pasará a ser un entorno cerrado dentro de algún tipo de edificio o instalación.
    -He utilizado un terrain para generar el terreno montañoso, he hecho un tunel que cruza la montaña y da paso a la segunda sección del      juego que es un edificio generado con prefabs de un pack de assets.
El personaje deberá tener dos tipos de arma. Una más pequeña, de cadencia lenta y precisa como podría ser una pistola, y otra más de largo alcance y de fuego rápido como podría ser una ametralladora.
    -He desarrollado dos GameObjects, uno con un render de un AK-47 este arma tiene una cadencia de fuego baja. También he desarrollado un arma con render de pistola con cadencia alta. Estas 2 armas disparan un prefab de una bala añadiendo fuerza al rigidbody de esta desde el extremo del cañon. Esta bala al inpactar con un enemigo lo matará. Las armas cuentan con munición limitada que se podra reponer con cajas de munición que encontraremos por el escenario. Las armas también tendras retroceso para darles un poco de realismo, debido a esto el arma con mayor cadencia de fuego pierde precisión.
A parte de su barra de vida, el personaje tendrá también una barra de escudo. Cuando le disparen, si tiene aún escudo, será el escudo quien reciba la mayor parte del daño, pero aun así deberá perder un poco de vida. Cuando el escudo llegue a 0, la vida se llevará el 100% del daño.
    -Cuando un enemigo te dispara en el caso de que tengas escudo este se llevará 20 puntos de daño y perderas 5 puntos de vida, en el caso de no tener escudo pierdes 20 puntos de vida directamente. El escudo y la vida también se pueden reponer con botiquines.
Tanto la cantidad de vida, cómo de escudo, el tipo de arma y la munición deberán siempre verse por pantalla (HUD).
    -Hecho.
El escenario deberá tener plataformas móviles, ya sean ascensores o desplazamientos horizontales.
    -Para acceder a la segunda zona del escenario que es el edifio, habrá que subir mediante un ascensor que se activa al subirse a el, el ascensor subirá hasta el punto indicado, y cuando el jugador ya no esté subido al ascensor este bajará a su posición inicial.
Deberán haber puertas que no puedan abrirse si el player no tiene la llave adecuada. Así se irán generando puzles.
    -En la primera zona nos encontramos una puerta cerada, para poder abrirla tendremos que derrotar al enemigo que hay en esta zona. El enemigo nos soltará una llave que al cogerla abrirá la puerta.
Deberá haber "enemigos"  que patrullen por la escena y que si te ven se pongan a dispararte. Si pasas cerca suyo pero no te han visto (simulando que han oído tus pasos) deberán dar una vuelta de 360 grados buscándote antes de seguir con su patrulla si no te ven.
    -Los enemigos hacen su ruta predefinida. Si entras en su radio de audición pero no estás en su campo de visión estos girarán hacia la derecha hasta encontrarte o hasta que salgas de su campo de audición. Si te tienen en su campo de visión te dispararán. Para detectar si estás en el campo de visión de un enemigo se traza un vector entre los 2 sujetos, si este vector no difiere más de 45 grados con el frente del enemigo estarás dentro del campo de visión.
Esparcidos por el escenario deberán haber ítems de 'vida', 'escudo' y 'munición'. Los enemigos al morir deberían dejar alguno en el suelo. También podrían dejar llaves.
    -Hecho.
El juego deberá tener pantalla de game over y que al morir podamos reiniciar el nivel.
    -Hecho

Enlace al video: https://www.youtube.com/watch?v=x5qdeGVdHIw
