﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UnityStandardAssets.Characters.FirstPerson
    {
    public class HealthPack : MonoBehaviour
    {
        
        public GameObject player;

        private void Awake() {
            player = GameObject.Find("FPSController 1");
        }

        private void OnTriggerEnter() {
            
                player.GetComponent<FirstPersonController>().vida += 20;

                Destroy(gameObject);

        }

    }
}