﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UnityStandardAssets.Characters.FirstPerson
{
    public class addAnmo : MonoBehaviour
    {   

        public GameObject player;

        private void Awake() {
            player = GameObject.Find("FPSController 1");
        }



        private void OnTriggerEnter() {

                player.GetComponent<FirstPersonController>().primaryy.GetComponent<Weapon>().anmo += 20;

                player.GetComponent<FirstPersonController>().primaryy.GetComponent<Weapon>().ac.changetext(player.GetComponent<FirstPersonController>().primaryy.GetComponent<Weapon>().maxanmo, player.GetComponent<FirstPersonController>().primaryy.GetComponent<Weapon>().anmo);


                Destroy(gameObject);

        }
    }
}