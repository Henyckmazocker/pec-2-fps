﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{

    public float vida = 100;

    public bool visto;
    public GameObject llave;

    public GameObject puertacerrada;
    public GameObject puertaabierta;

    public GameObject healthpack;
    public GameObject anmopack;

    public GameObject shieldPack;


    // Update is called once per frame
    void Update()
    {
        
        if(vida <= 0){
            muerte();
        }

    }

    public void muerte(){
        if(gameObject.name.Equals("EnemyKey")){
            
            GameObject coso = Instantiate(llave,transform.position, transform.rotation);
            coso.GetComponent<llaveController>().puertacerrada = puertacerrada;
            coso.GetComponent<llaveController>().puertaabierta = puertaabierta;

        }else{

            int num = Random.Range(1, 4);

            if(num == 1){
                GameObject coso = Instantiate(healthpack,transform.position, transform.rotation);
            }else if(num == 2){
                GameObject coso = Instantiate(anmopack,transform.position, transform.rotation);
            }else{
                GameObject coso = Instantiate(shieldPack,transform.position, transform.rotation);

            }

        }

        Destroy(gameObject);
        
    }

}
