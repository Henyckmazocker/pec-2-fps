﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class elevator : MonoBehaviour
{

    public GameObject coso;

    public bool funcionando = false;

    void Update(){

        if(!funcionando && coso.transform.position.y > 1f){

            coso.transform.position -= coso.transform.up * Time.deltaTime*5f; 

        }

    }

    private void OnTriggerStay(Collider player) {

        if(player.tag == "Player"){
            
            funcionando = true;
            coso.transform.position += coso.transform.up * Time.deltaTime*5f;

        }

    }

    private void OnTriggerExit(Collider other) {
        funcionando = false;
    }

}
