﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class llaveController : MonoBehaviour
{

    public GameObject puertacerrada;
    public GameObject puertaabierta;

    private void OnTriggerEnter(Collider other) {
        
        if(other.tag == "Player"){
            puertaabierta.SetActive(true);
            puertacerrada.SetActive(false);
            Destroy(gameObject);
        }


    }

}
