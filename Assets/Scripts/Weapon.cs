﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{

    public float cooldown;

    public float timeout = 0f;

    public int maxanmo;


    public int anmo;

    public GameObject bullet;

    public GameObject spawnbullet;

    public float bulletDamage = 2f;

    public float recoil = 0;

    public float maxRecoil_x = 80f;

    public float recoilSpeed = 10f;

    private Quaternion rotationbase;

    public AnmoController ac;

    void Start()
    {

        rotationbase = transform.localRotation;
        maxanmo = anmo;
    }

    void Update()
    {

        if(Input.GetButtonDown("Fire1") && anmo > 0 && timeout > cooldown){
            Fire();
            recoil += 0.1f;
        }
        
        timeout += Time.deltaTime;
        recoiling();
    }

    public void recoiling() {
        if(recoil > 0)
        {
            Quaternion  maxRecoil = Quaternion.Euler (-maxRecoil_x,0,0);
            // Dampen towards the target rotation
            transform.localRotation = Quaternion.Slerp (transform.localRotation, maxRecoil, Time.deltaTime * recoilSpeed);
            recoil -= Time.deltaTime;
        }
        else
        {
            recoil = 0;
            // Dampen towards the target rotation
            transform.localRotation = Quaternion.Slerp (transform.localRotation, rotationbase, Time.deltaTime * recoilSpeed / 2);
        }
    }

    private void Fire(){

        GameObject bulletins = Instantiate(bullet,spawnbullet.transform.position, transform.rotation);

        anmo--;

        ac.changetext(maxanmo, anmo);

        timeout = 0f;

    }

}