﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityStandardAssets.Characters.FirstPerson;

public class enemyAIDemo : MonoBehaviour
{
    [HideInInspector] public IdleStateDemo    idleState;
    [HideInInspector] public AlertStateDemo   alertState;
    [HideInInspector] public IEnemyStateDemo  currentState;

    public List<Transform> posi;

    public Material myMaterial;

    public int step = 1;

    void Start()
    {
        // Creamos los estados de nuestra IA.
        idleState = new IdleStateDemo(this);
        alertState  = new AlertStateDemo(this);

        // Le decimos que inicialmente empezará en Idle
        currentState = idleState;
    }
    
    void Update()
    {

        if(transform.position.x == posi[step].position.x && transform.position.z == posi[step].position.z){
            step++;
            if(step == posi.Count){
                step = 0;
            }
        }

        if(!currentState.ToString().Equals("AlertStateDemo")){

            gameObject.GetComponent<Transform>().position = Vector3.MoveTowards(transform.position, new Vector3(posi[step].position.x,transform.position.y,posi[step].position.z),5*Time.deltaTime);

        }
        // Como nuestros estados no heredan de
        // MonoBehaviour, no se llama a su update 
        // automáticamente, y nos encargaremos 
        // nosotros de llamarlo a cada frame.
        currentState.UpdateState();
    }

    // Ya que nuestros states no heredan de 
    // MonoBehaviour, tendremos que avisarles
    // cuando algo entra, está o sale de nuestro
    // trigger.
    void OnTriggerEnter(Collider col)
    {
        currentState.OnTriggerEnter(col);
    }

    void OnTriggerStay(Collider col)
    {
        currentState.OnTriggerStay(col);
    }

    void OnTriggerExit(Collider col)
    {
        currentState.OnTriggerExit(col);
    }
}
