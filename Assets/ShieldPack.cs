﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UnityStandardAssets.Characters.FirstPerson
    {
public class ShieldPack : MonoBehaviour
{
    // Start is called before the first frame update
      public GameObject player;

        private void Awake() {
            player = GameObject.Find("FPSController 1");
        }

        private void OnTriggerEnter() {
            
                player.GetComponent<FirstPersonController>().shield += 20;

                Destroy(gameObject);

        }
}
    }