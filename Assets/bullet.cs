﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bullet : MonoBehaviour
{
    // Start is called before the first frame update

    void Start()
    {
        GetComponent<Rigidbody>().AddForce(transform.forward * 100f,ForceMode.Impulse);
    }

    // Update is called once per frame
    void Update()
    {


    }

    public void OnCollisionEnter(Collision other){


        if(other.gameObject.name == "Head"){
            other.gameObject.transform.parent.parent.parent.GetComponent<EnemyController>().vida = 0;
                    Destroy(gameObject);
                    gameObject.GetComponent<ParticleSystem>().Pause();

        }else{

            if(other.gameObject.name == "Cylinder"){

                other.gameObject.transform.parent.parent.GetComponent<EnemyController>().vida -= 20;
                    Destroy(gameObject);
                    gameObject.GetComponent<ParticleSystem>().Pause();
            }else if(other.gameObject.name != "Enemy"){
                    Destroy(gameObject);
                    gameObject.GetComponent<ParticleSystem>().Pause();
            }


        }



    }

}
